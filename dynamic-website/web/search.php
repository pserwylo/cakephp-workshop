<?php

include '../lib/db.php';

$query = isset($_GET['query']) ? $_GET['query'] : '';
$results = array();

if ($query) {
    $result = mysqli_query($db, "SELECT * FROM products where name LIKE '%$query%'");
    while ($row = mysqli_fetch_assoc($result)) {
        $results[] = $row;
    }
}

?>
<html>
<body>
  <h1>Search</h1>
  <form method="get">
    <label for="search">Query</label>
    <input name="query" id="query" value="<?php echo $query ?>" />
  </form>
  <?php if ($query) { ?>
    <h2>Results for "<?php echo $query ?>"</h2>
      <?php if (!$results) { ?>
          No results
      <?php } else { ?>
      <ul>
        <?php foreach($results as $result) { ?>
          <li><?php echo $result['name']; ?></li>
        <?php } ?>
      </ul>
    <?php } ?>
  <?php } ?>
</body>
</html>
