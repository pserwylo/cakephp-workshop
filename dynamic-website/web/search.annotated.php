<?php

// We have to know where this "Search" file lives, where the "db.php" file lives, and how to get from one to the other.
// WHat if the file is run twice? Will it end up having weird unintended side effects?
include '../lib/db.php';

// This is dealing a bit too much with the HTTP layer. WHat happens if we accidentally clear the contents of $_GET['query']?
$query = isset($_GET['query']) ? $_GET['query'] : '';
$results = array();

if ($query) {
    // We don't want to have to write our own SQL code.
    // This is vulnerable to an SQL injection attack because we didn't sanitize the $query variable, or use parameter binding in the query.
    $result = mysqli_query($db, "SELECT * FROM products where name LIKE '%$query%'");

    // The way in which we pull records out of the database is very procedural. That is, we are providing instructions
    // on **what PHP should do to achieve our goal**, not **explaining what our goal actually is**.
    while ($row = mysqli_fetch_assoc($result)) {
        $results[] = $row;
    }
}

?>
<!--
Now we have a bunch of HTML code mixed in with our business logic above.
These really should be separated, so that the business logic doesn't need to know about whcih view it is rendered in.
What if we want to do the same search for products, but instead, we want to render them as a .CSV for export?
We should be able to do this by hooking the exact same business logic up to a different type of view.

Note though, that this is probably a cleaner separation of business logic and templating than many oldschool PHP apps.
Traditionally, you would see things like "input name='query' value='php echo $_GET['query']'", and perhaps the
database query would also be intermingled with HTML code.
-->
<html>
<body>
  <h1>Search</h1>
  <form method="get">
    <label for="search">Query</label>
    <input name="query" id="query" value="<?php echo $query ?>" />
  </form>
  <?php if ($query) { ?>
    <h2>Results for "<?php echo $query ?>"</h2>
      <?php if (!$results) { ?>
          No results
      <?php } else { ?>
      <ul>
        <?php foreach($results as $result) { ?>
          <li><?php echo $result['name']; ?></li>
        <?php } ?>
      </ul>
    <?php } ?>
  <?php } ?>
</body>
</html>
