# Websites with dynamic content

There is no rule that says the web server needs to return the contents of a file that it found.
Indeed, most of the time a website is not doing that, but rather executing the code found in the file, and then returning the _output of that execution_.

## Setting up this website

This website needs to connect to a database on your local machine.
It is hard coded to look for a MySQL database called `oldschool`, a user account `oldschool`, with a password of `oldschool`.

(Note: This is intentionally dumb, I wanted to illustrate a poorly designed website which hard codes database details in the code).

### Database setup

The database can be created with the following SQL:

```
CREATE DATABASE oldschool;
GRANT ALL ON oldschool.* TO 'oldschool'@'localhost' IDENTIFIED BY 'oldschool';
```

Then executing the SQL in the `db.sql` file in this repository.

## Running this website

To test it, start a web server in this directory, e.g. by running the command:

```
php -S localhost:8765 -t web/
```

(The `-t web/` tells the web server that this is where to look for files which match the incoming request).

Now navigate to `http://localhost:8765/search.php?query=ee` in your web browser.

## Diving deeper

The web server is _now_ responsibile for listening for HTTP requests, _finding_ the relevant files, _executing the code found in the files_, then _returning the output of that execution_.

We can see this by using `telnet` again.

```
telnet localhost 8765
GET /search.php?query=ee HTTP/1.1

```

(Note the extra new line at at the end, because HTTP requests are considered finished when there are two consecutive new lines).

If you check out the console where the PHP built-in web server is running (e.g. where you executed `php -S localhost:8765 -t web`), you will see that it received the request for `/search.php?query=ee`:

```
[Fri Apr  6 11:20:57 2018] 127.0.0.1:42818 [200]: /search.php?query=ee
```

Additionally, you will see in your `telnet` window that you got a HTTP response:

```
HTTP/1.1 200 OK
Date: Fri, 06 Apr 2018 11:20:57 +1000
Connection: close
X-Powered-By: PHP/7.1.15-0ubuntu0.17.10.1
Content-type: text/html; charset=UTF-8

<html>
<body>
  <h1>Search</h1>
  <form method="get">
    <label for="search">Query</label>
    <input name="query" id="query" value="ee" />
  </form>
      <h2>Results for "ee"</h2>
            <ul>
                  <li>Cheese</li>
                  <li>Beer</li>
              </ul>
      </body>
</html>
Connection closed by foreign host.
```
