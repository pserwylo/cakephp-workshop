# Websites with dynamic content

Modern CakePHP based website created using:

```
composer create-project --prefer-dist cakephp/app modern-website
```

## Running the website

Essentailly we will still be encouraging use of the built in webserver, except this time it will use a helper script from cake:

```
bin/cake server
```
