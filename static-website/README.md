# Websites with only static HTML

This is an example website made of static HTML pages.

## Running this website

To test it, start a web server in this directory, e.g. by running the command:

```
php -S localhost:8765
```

And then navigating to `http://localhost:8765` or `http://lcoalhost:8765/products.html` in your web browser.

## Diving deeper

The web server is responsibile for listening for HTTP requests, fetching the relevant files, then returning the contents of that file verbatim.

We can see this by using `telnet`, which literaly sends plain text to a web server, and then gets the response back:

```
telnet localhost 8765
GET /index.html HTTP/1.1

```

(Note the extra new line at at the end, because HTTP requests are considered finished when there are two consecutive new lines).

If you check out the console where the PHP built-in web server is running (e.g. where you executed `php -S localhost:8765`), you will see that it received the request for `/index.html`:

```
[Fri Apr  6 11:04:27 2018] 127.0.0.1:42252 [200]: /index.html
```

Additionally, you will see in your `telnet` window that you got a HTTP response:

```
HTTP/1.1 200 OK
Date: Fri, 06 Apr 2018 11:04:27 +1000
Connection: close
Content-Type: text/html; charset=UTF-8
Content-Length: 128

<html>
<body>
  <h1>Oldschool PHP website</h1>
  <ul>
    <li><a href="products.html">Products</a></li>
  </ul>
</body>
</html>
Connection closed by foreign host.
```
